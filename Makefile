IRIS_COMMIT_HASH=0d2f193a5caa6b4d7f9a68e054bc10f8ef2fb1c4
IRIS_SOURCE_URL=https://github.com/linkedin/iris/archive/${IRIS_COMMIT_HASH}.zip

build:
	podman build --tag iris:latest \
		--build-arg IRIS_COMMIT_HASH=${IRIS_COMMIT_HASH} \
		--build-arg IRIS_SOURCE_URL=${IRIS_SOURCE_URL} \
		--label org.opencontainers.image.created="$$(date -I seconds)" \
		--label org.opencontainers.image.version="${IRIS_COMMIT_HASH}" \
		--label org.opencontainers.image.revision="${IRIS_COMMIT_HASH}" \
		-f Dockerfile . 
kics:
	podman run --rm -v $PWD:/path checkmarx/kics scan --no-progress -p "/path" -o "/path/"
