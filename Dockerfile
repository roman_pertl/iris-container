FROM alpine:3.15 as source

ARG IRIS_COMMIT_HASH
ARG IRIS_SOURCE_URL
RUN apk update && apk add --no-cache wget && wget -O archive.zip ${IRIS_SOURCE_URL} && \
    unzip archive.zip && rm archive.zip && mv iris-${IRIS_COMMIT_HASH} /home/iris
WORKDIR /home/iris
# Fix broken file permissions from the zip file (direcotries are group and world writable)
RUN chmod -R og-w .
# patch source to configure sender rpc log file to a writeable location
RUN  sed -i "s/filename.*/filename: '\/home\/iris\/var\/log\/sender\/rpc.access.log'/" configs/config.dev.yaml

FROM ubuntu:20.04

LABEL org.opencontainers.image.authors="Roman Pertl <roman@pertl.org>"
LABEL org.opencontainers.image.url="https://gitlab.com/roman_pertl/iris-container"
LABEL org.opencontainers.image.documentation="https://gitlab.com/roman_pertl/iris-container"
LABEL org.opencontainers.image.source="https://github.com/linkedin/iris"
LABEL org.opencontainers.image.vendor="LinkedIn"
LABEL org.opencontainers.image.licenses="BSD-2"
LABEL org.opencontainers.image.title="Iris"
LABEL org.opencontainers.image.base.name="ubuntu:20.04"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::=--force-confold dist-upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::=--force-confold install \
    libffi-dev libsasl2-dev python3-dev libyaml-dev \
    libldap2-dev libssl-dev python3-pip python3-setuptools python3-venv \
    mysql-client nginx uwsgi uwsgi-plugin-python3 uwsgi-plugin-gevent-python3 && \
    pip3 --no-cache-dir install mysql-connector-python && \
    rm -rf /var/cache/apt/archives/*


WORKDIR /home/iris

COPY --from=source /home/iris/src source/src
COPY --from=source /home/iris/setup.py /home/iris/MANIFEST.in /home/iris/README.md /home/iris/LICENSE source/

RUN python3 -m venv /home/iris/env && \
    /bin/bash -c 'source /home/iris/env/bin/activate && python3 -m pip install --no-cache-dir -U pip wheel && cd /home/iris/source && pip --no-cache-dir install -e ".[prometheus]"'

COPY --from=source /home/iris/ops/daemons daemons/
COPY --from=source /home/iris/ops/daemons/uwsgi-docker.yaml daemons/uwsgi.yaml
COPY --from=source /home/iris/db db/
COPY --from=source /home/iris/configs/config.dev.yaml config/config.yaml
COPY --from=source /home/iris/healthcheck /tmp/status
COPY --from=source /home/iris/ops/entrypoint.py entrypoint.py

RUN useradd -m -s /bin/bash iris && \
    chown -R iris:iris /var/log/nginx /var/lib/nginx && \
    mkdir -p /home/iris/var/log/uwsgi /home/iris/var/log/nginx /home/iris/var/run /home/iris/var/relay /home/iris/var/log/sender /home/iris/source/src/iris/ui/static/.webassets-cache /home/iris/source/src/iris/ui/static/bundles && \
    chown -R iris:iris /home/iris/var/log/uwsgi /home/iris/var/log/nginx /home/iris/var/run /home/iris/var/relay /home/iris/var/log/sender /home/iris/source/src/iris/ui/static/.webassets-cache /home/iris/source/src/iris/ui/static/bundles

EXPOSE 16649

ENV INIT_FILE=/tmp/iris_db_initialized
USER iris
CMD ["bash", "-c", "source /home/iris/env/bin/activate && exec python -u /home/iris/entrypoint.py"]
